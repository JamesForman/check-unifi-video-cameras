#!/usr/bin/python

"""
Check all configured cameras on a Ubiquiti UniFi Video server and confirm
the passwords are non-default.

Andrew Ruthven <andrew@etc.gen.nz>.

Version: 1.0.0
"""

import time
import datetime
import argparse
import sys
import requests
import paramiko

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

COUNT_OK = 0
COUNT_WARNING = 0
COUNT_CRITICAL = 0
COUNT_UNKNOWN = 0
COUNT_IGNORED = 0
COUNT_TOTAL = 0

SERVER = "127.0.0.1"
PORT = 7443
IGNORE = "/etc/unifi-camera-ignore.txt"

if __name__ == "__main__":

    # Optional/Required arguements: http://bugs.python.org/issue9694
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--server', default=SERVER, help='Server to connect to, defaults to {0}'.format(SERVER))
    parser.add_argument('--port', default=PORT, type=int, help='Port to connect to, defaults to {0}'.format(PORT))
    parser.add_argument('--apikey', help='API Key to use, no default')
    parser.add_argument('--debug', default=False, help='To print debug information set to "True"')
    parser.add_argument('--ignore', default=IGNORE, help='A file of UUIDs to ignore, defaults to {0}'.format(IGNORE))
    args = parser.parse_args()

    DEBUG = args.debug
    URL = "https://{0}:{1}/api/2.0/camera/?apiKey={2}".format(args.server, args.port, args.apikey)

    api_data = requests.get(URL, verify=False)
    json_data = (api_data.json())
    cameras = json_data["data"]

    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
 

    for camera in cameras:
        if DEBUG:
            print("Evaluating camera with UUID: {0}".format(camera["uuid"]))
        COUNT_TOTAL += 1

        try:
            if camera["uuid"] in open(args.ignore).read():
                if DEBUG:
                    print("Camera '{0}' ({1}) is in the ignore file".format(camera["name"], camera["model"]))
                COUNT_IGNORED += 1
            else:

                if camera["state"] == "CONNECTED":
                    if DEBUG:
                        print("Camera '{0}' - IP: {1}".format(camera["name"], camera["host"]))

                    try:
                        ssh.connect(camera["host"], username="ubnt", password="ubnt")
                        ssh_stdin, ssh_stdout, ssh_stderr = ssh.exec_command("hostname")
                        print("Camera '{0}' has the default password set".format(camera["name"]))

                        COUNT_CRITICAL += 1
                    except TimeoutError:
                        print("Camera '{0}' timed out trying to connect".format(camera["name"]))
                        COUNT_WARNING += 1
                    except paramiko.ssh_exception.AuthenticationException:
                        COUNT_OK += 1
                else:
                    print("Camera '{0}' is not connected".format(camera["name"]))
                    COUNT_WARNING += 1

        except FileNotFoundError:
            if DEBUG:
                print("ERROR: No ignore file ({0}) found".format(args.ignore))
            raise

        if DEBUG:
            print("")

    if COUNT_IGNORED > 0:
        if DEBUG:
            print("{0}/{1} cameras were ignored".format(COUNT_IGNORED, COUNT_TOTAL))
        COUNT_TOTAL = COUNT_TOTAL - COUNT_IGNORED
    if COUNT_CRITICAL > 0:
        print("{0}/{1} cameras are in a CRITICAL state".format(COUNT_CRITICAL, COUNT_TOTAL))
    if COUNT_WARNING > 0:
        print("{0}/{1} cameras are in a WARNING state".format(COUNT_WARNING, COUNT_TOTAL))
    if COUNT_UNKNOWN > 0:
        print("{0}/{1} cameras are in an UNKNOWN state".format(COUNT_UNKNOWN, COUNT_TOTAL))
    if COUNT_OK > 0:
        print("{0}/{1} cameras are in an OK state".format(COUNT_OK, COUNT_TOTAL))

    if DEBUG:
        print("")

    if COUNT_CRITICAL > 0:
        if DEBUG:
            print("State: Critical")
        sys.exit(2)
    elif COUNT_WARNING > 0:
        if DEBUG:
            print("State: Warning")
        sys.exit(1)
    elif COUNT_UNKNOWN > 0:
        if DEBUG:
            print("State: Unknown")
        sys.exit(3)
    else:
        if DEBUG:
            print("State: OK")
        sys.exit(0)
