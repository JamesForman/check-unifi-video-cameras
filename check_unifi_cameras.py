#!/usr/bin/python

"""
Check that a selected camera is connected to the server and set to record video.
For use with Ubiquiti's UniFi Video from version 3.9.9 onwards.

James Forman <james@jamesforman.co.nz>.

Version: 1.0.0
"""

import time
import datetime
import argparse
import sys
import json
import requests

from requests.packages.urllib3.exceptions import InsecureRequestWarning
requests.packages.urllib3.disable_warnings(InsecureRequestWarning)

COUNT_OK = 0
COUNT_WARNING = 0
COUNT_CRITICAL = 0
COUNT_UNKNOWN = 0
COUNT_IGNORED = 0
COUNT_TOTAL = 0

SERVER = "127.0.0.1"
PORT = 7443
IGNORE = "/etc/unifi-camera-ignore.txt"

if __name__ == "__main__":

    # Optional/Required arguements: http://bugs.python.org/issue9694
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('--server', default=SERVER, help='Server to connect to, defaults to {0}'.format(SERVER))
    parser.add_argument('--port', default=PORT, type=int, help='Port to connect to, defaults to {0}'.format(PORT))
    parser.add_argument('--apikey', help='API Key to use, no default')
    parser.add_argument('--recording', default=False, help='State of recording, defaults to "False"')
    parser.add_argument('--debug', default=False, help='To print debug information set to "True"')
    parser.add_argument('--ignore', default=IGNORE, help='A file of UUIDs to ignore, defaults to {0}'.format(IGNORE))
    parser.add_argument('--show-uuid', action='store_true', help='Show UUIDs of troublesome cameras')
    args = parser.parse_args()

    DEBUG = args.debug
    URL = "https://{0}:{1}/api/2.0/camera/?apiKey={2}".format(args.server, args.port, args.apikey)

    try:
        api_data = requests.get(URL, verify=False)
    except requests.exceptions.RequestException:
        print('Unable to contact UniFi server')
        sys.exit(2)

    if api_data.status_code != 200:
        if api_data.status_code == 401:
            print('Failed to authenticate to UniFi server, is your API key good?')
            sys.exit(2)

        print('Received an unexpected response code from UniFi server: {0}'.format(api_data.status_code))
        sys.exit(2)

    try:
        json_data = (api_data.json())
    except json.decoder.JSONDecodeError:
        print('Unable to parse JSON from UniFi server')
        sys.exit(2)

    cameras = json_data["data"]

    for camera in cameras:
        if DEBUG:
            print("Evaluating camera with UUID: {0}".format(camera["uuid"]))
        COUNT_TOTAL = COUNT_TOTAL + 1

        try:
            if camera["uuid"] in open(args.ignore).read():
                if DEBUG:
                    print("Camera '{0}' ({1}) is in the ignore file".format(camera["name"], camera["model"]))
                COUNT_IGNORED = COUNT_IGNORED + 1
            else:
                camera_uuid = ''
                if args.show_uuid:
                    camera_uuid = ' - {0}'.format(camera["uuid"])

                if camera["state"] == "CONNECTED":
                    camera_uptime = int(time.time() - camera["uptime"] / 1000)

                    if args.recording is False:
                        if camera["recordingSettings"]["motionRecordEnabled"] is True:
                            if DEBUG:
                                print("Camera is set to do motion recording")
                        elif camera["recordingSettings"]["fullTimeRecordEnabled"] is True:
                            if DEBUG:
                                print("Camera is set to do full time recording")
                        else:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}'{1} is not set to record".format(camera["name"], camera_uuid))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    elif args.recording == "motionRecord":
                        if camera["recordingSettings"]["motionRecordEnabled"] is False:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}'{1} is not set to record on motion".format(camera["name"], camera_uuid))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    elif args.recording == "fullTimeRecord":
                        if camera["recordingSettings"]["fullTimeRecordEnabled"] is False:
                            if DEBUG:
                                print("State: CRITICAL")
                            print("Camera '{0}'{1} is not set to record full time".format(camera["name"], camera_uuid))
                            COUNT_CRITICAL = COUNT_CRITICAL + 1
                    else:
                        if DEBUG:
                            print("State: CRITICAL")
                        print("Camera '{0}'{1} is not set to record".format(camera["name"], camera_uuid))
                        COUNT_CRITICAL = COUNT_CRITICAL + 1

                    if camera_uptime <= 3600:
                        if DEBUG:
                            print("State: WARNING")
                        print("Camera '{0}'{1} ({2}) has been connected for less than an hour".format(camera["name"], camera_uuid, camera["model"]))
                        COUNT_WARNING = COUNT_WARNING + 1
                    else:
                        if DEBUG:
                            print("State: OK")
                            human_camera_uptime = str(datetime.timedelta(seconds=camera_uptime))
                            print("Camera: '{0}'{1} ({2}) has been connected for {3}".format(camera["name"], camera_uuid, camera["model"], human_camera_uptime))
                        COUNT_OK = COUNT_OK + 1

                else:
                    if DEBUG:
                        print("State: CRITICAL")
                    print("Camera: '{0}'{1} ({2}) is not connected".format(camera["name"], camera_uuid, camera["model"]))
                    COUNT_CRITICAL = COUNT_CRITICAL + 1

        except FileNotFoundError:
            if DEBUG:
                print("ERROR: No ignore file ({0}) found".format(args.ignore))
            raise

        if DEBUG:
            print("")

    if COUNT_IGNORED > 0:
        if DEBUG:
            print("{0}/{1} cameras were ignored".format(COUNT_IGNORED, COUNT_TOTAL))
        COUNT_TOTAL = COUNT_TOTAL - COUNT_IGNORED
    if COUNT_CRITICAL > 0:
        print("{0}/{1} cameras are in a CRITICAL state".format(COUNT_CRITICAL, COUNT_TOTAL))
    if COUNT_WARNING > 0:
        print("{0}/{1} cameras are in a WARNING state".format(COUNT_WARNING, COUNT_TOTAL))
    if COUNT_UNKNOWN > 0:
        print("{0}/{1} cameras are in an UNKNOWN state".format(COUNT_UNKNOWN, COUNT_TOTAL))
    if COUNT_OK > 0:
        print("{0}/{1} cameras are in an OK state".format(COUNT_OK, COUNT_TOTAL))

    if DEBUG:
        print("")

    if COUNT_CRITICAL > 0:
        if DEBUG:
            print("State: Critical")
        sys.exit(2)
    elif COUNT_WARNING > 0:
        if DEBUG:
            print("State: Warning")
        sys.exit(1)
    elif COUNT_UNKNOWN > 0:
        if DEBUG:
            print("State: Unknown")
        sys.exit(3)
    else:
        if DEBUG:
            print("State: OK")
        sys.exit(0)
